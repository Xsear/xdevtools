
-- ------------------------------------------
-- Dev Tools 2016-05-15
--   by: Xsear
-- ------------------------------------------

require "unicode"
require "math"
require "table"
require 'lib/lib_Debug'
require 'lib/lib_Items'
require 'lib/lib_Slash'
require 'lib/lib_ChatLib'
require "lib/lib_Callback2"
require "lib/lib_Wallet"

-- ------------------------------------------
-- CONSTANTS
-- ------------------------------------------
SOUND_TARGET_NONE = 'Play_UI_Beep_26'
SOUND_TARGET_INVALID = 'Play_UI_Beep_35'
SOUND_TARGET_SEARCHING = 'Play_UI_Beep_12'
SOUND_TARGET_MISSING = 'Play_UI_Beep_10'
SOUND_TARGET_EXISTS = 'Play_UI_Beep_06'
SOUND_TARGET_PLAYER = 'Play_Vox_Emote_Groan'
SOUND_TARGET_LOOT = 'Play_SFX_UI_Loot_Flyover'


function OnComponentLoad()
    Debug.EnableLogging(true)

    -- Info Lookup 
    LIB_SLASH.BindCallback({slash_list='typeid, itemTypeId, typeId, itemtypeid, typeinfo, typeInfo, sdbid, sdb_id, SDB_ID', description='Log itemTypeInfo of provided itemTypeId to console', func=typeId})
    LIB_SLASH.BindCallback({slash_list='subTypeId, subtypeid, subid, stid', description='Log typeInfo of provided subTypeId info to console', func=subTypeId})
    LIB_SLASH.BindCallback({slash_list='getmytargetid, getMyTargetId, playerId, myid, myId', description='Log player entity id to console', func=getMyTargetId})
    LIB_SLASH.BindCallback({slash_list='zoneinfo,zoneId,zoneid,zid,zoneInfo,getmyzone,getzone', description='Log zoneInfo of provided zoneId, or current zone id, to console', func=ZoneInfo})
    LIB_SLASH.BindCallback({slash_list='abilityId, abilityid, abilityInfo, abilityInfo, aInfo', description='Log abilityInfo of provided abilityId to console', func=abilityId})
    LIB_SLASH.BindCallback({slash_list='itemId, guid, itemguid, itemid, itemInfo, iteminfo, playeritem, playerItem', description='Log itemInfo of provided item guid to console', func=itemId})
    LIB_SLASH.BindCallback({slash_list='entinfo', description='Log info about provided target to console.', func=DumpEntity})
    LIB_SLASH.BindCallback({slash_list = "mapmarker,mmarker,markerId,markerid", description = "Log info about provided mapmarker to console.", func = markerId})
    LIB_SLASH.BindCallback({slash_list = "encounter,encid,encounterid,encounterId", description = "Log info about provided mapmarker to console.", func = encounterId})
    --LIB_SLASH.BindCallback({slash_list='craftingTypeId, craftingtypeid, craftid, crafttypeid, ctid', description='xdev', func=craftingTypeId})

    -- Inspection
    LIB_SLASH.BindCallback({slash_list='ri, inspect, i', description='Log info of recticle target to console', func=RectileInspect})
    LIB_SLASH.BindCallback({slash_list='loc, ret, pos', description='Log coordinates of recticle to console', func=AimPos})

    -- Info display
    LIB_SLASH.BindCallback({slash_list='link, aslink', description='Output an item chat link from provided itemTypeId, can also add modules with subsequent typeIds', func=AsLink})
    
    -- Searching
    LIB_SLASH.BindCallback({slash_list='search, src, query, searchItems', description='Iterate all items and log items with names that contain the provided query string to the console.', func=SearchInAllItemsByName})
    LIB_SLASH.BindCallback({slash_list='find, finditem, findItem, findexact, findExact', description='Iterate all items and log those where the name exactly matches the query string to the console.', func=FindInAllItemsByName})
    LIB_SLASH.BindCallback({slash_list='searchinv, iteminv, inventoryitem, invitem, invitemsearch, invsrc, srcinv', description='Iterate inventory items and log items with names that contain the provided query string to the console.', func=SearchInInventoryByName})
    LIB_SLASH.BindCallback({slash_list='findinv, invfind, exactinv, invitemname', description='Iterate inventory items and log those where the name exactly matches the query string to the console.', func=FindInInventoryByName})
    LIB_SLASH.BindCallback({slash_list='entscan, scanent', description='Log info about available targets to console.', func=ScanEntity})
end


-- ------------------------------------------
-- UTILITY/RETURN FUNCTIONS
-- ------------------------------------------

function Output(text)
    local args = {
        text = tostring(text),
    }

    ChatLib.SystemMessage(args);
end

function _table.empty(table)
    if not table or next(table) == nil then
       return true
    end
    return false
end

function string(o)
    return '"' .. tostring(o) .. '"'
end
 
function recurse(o, indent)
    if indent == nil then indent = '' end
    local indent2 = indent .. '  '
    if type(o) == 'table' then
        local s = indent .. '{' .. '\n'
        local first = true
        for k,v in pairs(o) do
            if first == false then s = s .. ', \n' end
            if type(k) ~= 'number' then k = string(k) end
            s = s .. indent2 .. '[' .. k .. '] = ' .. recurse(v, indent2)
            first = false
        end
        return s .. '\n' .. indent .. '}'
    else
        return string(o)
    end
end
 
function var_dump(...)
    local args = {...}
    if #args > 1 then
        var_dump(args)
    else
        log(recurse(args[1]))
    end
end

function FilterZonesInstances()
    local filtered = {}
    local routes = {}
    local routes_raw = Game.ListRoutes()
    
    for i=1, #routes_raw do
        local zoneid = tonumber(routes_raw[i])
        routes[zoneid] = true
    end
    
    for idx, zoneinfo in ipairs(g_ZoneList) do
        local route_exists = routes[zoneinfo.zone_id]
        if route_exists then
            table.insert(filtered, _table.copy(zoneinfo))
        end
    end
    
    return filtered
end

function getAllItemsByTypeId()

    -- Prepare results
    local itemsByTypeId = {}

     -- Get all inventory items
    local inventoryItems, _ = Player.GetInventory(nil, false)

    -- Loop through each inventory item
    for i, inventoryItem in ipairs(inventoryItems) do

        local typeId = inventoryItem.item_sdb_id

        -- Create table if neccessary
        if not itemsByTypeId[typeId] then
            itemsByTypeId[typeId] = {}
        end 

        -- Insert    
        itemsByTypeId[typeId][#itemsByTypeId[typeId] + 1] = inventoryItem.item_id -- possibly userdata
    end

    return itemsByTypeId
end

function getItemsByTypeId(itemTypeId)

    -- Handle arguments
    itemTypeId = tostring(itemTypeId)
    
    -- Prepare results
    local results = {}

     -- Get all inventory items
    local inventoryItems, _ = Player.GetInventory(nil, false)

    -- Loop through each inventory item
    for i, inventoryItem in ipairs(inventoryItems) do
        local typeId = tostring(inventoryItem.item_sdb_id)

        -- If match, store
        if typeId == itemTypeId then
            results[#results + 1] = inventoryItem.item_id -- possibly userdata
        end
    end

    --[[
    -- Return nil if empty
    if not next(results) then
        return nil
    else
        return results
    end
    --]]

    -- May return empty table
    return results
end 








-- ------------------------------------------
-- USEFUL COMMANDS
-- ------------------------------------------

function SearchInAllItemsByName(args)
    Debug.Divider()
    Debug.Table("SearchInAllItemsByName", args)
    Debug.Divider()

    local searchQuery = unicode.lower(args.text)

    local maxNum = 300000
    local getItemInfo = Game.GetItemInfoByType 

    for num = 1, maxNum do
        local itemInfo = getItemInfo(num)
        if itemInfo and not _table.empty(itemInfo) then
            if unicode.match(unicode.lower(itemInfo.name), searchQuery) ~= nil then
                Debug.Log(itemInfo.itemTypeId .. "  |  " .. itemInfo.name)
            end
        end
    end

    Debug.Divider()
    Debug.Log("SearchInAllItemsByName end")
    Debug.Divider()
end


function FindInAllItemsByName(args)
    Debug.Divider()
    Debug.Table("FindInAllItemsByName", args)
    Debug.Log('ByExactName')
    Debug.Divider()

    local searchName = args.text

    local maxNum = 300000
    local getItemInfo = Game.GetItemInfoByType 

    for num = 1, maxNum do
        local itemInfo = getItemInfo(num)
        if itemInfo and not _table.empty(itemInfo) then
            if itemInfo.name == searchName then
                Debug.Table(itemInfo)
            end
        end
    end

    Debug.Divider()
    Debug.Log("FindInAllItemsByName end")
    Debug.Divider()
end

function SearchInInventoryByName(args)
    Debug.Divider()
    Debug.Table("SearchInInventoryByName", args)
    Debug.Divider()

    local name = unicode.lower(args.text)

    -- Get all inventory items
    local inventoryItems, _ = Player.GetInventory(nil, false)

    -- Loop through each inventory item
    for i, inventoryItem in ipairs(inventoryItems) do

        -- Check
        if unicode.match(unicode.lower(inventoryItem.name), name) ~= nil then
            Debug.Table({inventoryItem = inventoryItem})
            Debug.Log("itemTypeId" .. inventoryItem.item_sdb_id)
            Debug.Table({rootInfo = Player.GetItemInfo(inventoryItem.item_id)})
        end
    end

    Debug.Divider()
    Debug.Log("SearchInInventoryByName end")
    Debug.Divider()
end

function FindInInventoryByName(args)
    Debug.Divider()
    Debug.Table("FindInInventoryByName", args)
    Debug.Log('ByExactName')
    Debug.Divider()

    local name = unicode.lower(args.text)

    -- Get all inventory items
    local inventoryItems, _ = Player.GetInventory(nil, false)

    -- Loop through each inventory item
    for i, inventoryItem in ipairs(inventoryItems) do

        -- Check
        if inventoryItem.name == name then
            Debug.Table({inventoryItem = inventoryItem})
            Debug.Log("itemTypeId" .. inventoryItem.item_sdb_id)
            Debug.Table({rootInfo = Player.GetItemInfo(inventoryItem.item_id)})
        end
    end

    Debug.Divider()
    Debug.Log("FindInInventoryByName end")
    Debug.Divider()
end







function RectileInspect(args)

    local debugOutput = {}

    -- Get reticle info
    local reticleInfo = Player.GetReticleInfo()

    -- If reticle not targeting an entity exit
    if not reticleInfo or not reticleInfo.entityId then
        System.PlaySound(SOUND_TARGET_NONE)
        return
    end

    debugOutput['reticleInfo'] = reticleInfo

    -- Get ret location
    local aimPosition = Player.GetAimPosition()
    debugOutput['aimPosition'] = aimPosition

    -- Get entity target info
    local targetInfo = Game.GetTargetInfo(reticleInfo.entityId)

    -- If no target info exit
    if not targetInfo then
        System.PlaySound(SOUND_TARGET_INVALID)
        return
    end

    debugOutput['targetInfo'] = targetInfo

    debugOutput['targetVitals'] = Game.GetTargetVitals(reticleInfo.entityId)

    -- If npc
    if targetInfo.isNpc then
        System.PlaySoundOnActor(SOUND_TARGET_PLAYER, reticleInfo.entityId)

        debugOutput['characterInfo'] = Game.GetTargetCharacterInfo(reticleInfo.entityId)
    end

    -- If item
    if targetInfo.itemTypeId then 
        System.PlaySound(SOUND_TARGET_LOOT)

        debugOutput['itemInfo'] = Game.GetItemInfoByType(targetInfo.itemTypeId)
    end

    -- Play sound to acknowledge that we've hit a valid target
    System.PlaySound(SOUND_TARGET_EXISTS)

    -- Output debug info
    Debug.Table(debugOutput)
end



function AimPos()
    Debug.Table("AimPos " .. Player.GetAimPosition()) 
end

function AsLink(args)
    local itemTypeId = args[1] or 0
    local modules = {}

    if args[2] then
        modules[#modules+1] = args[2]
    end

    if args[3] then
        modules[#modules+1] = args[3]
    end

    if args[4] then
        modules[#modules+1] = args[4]
    end

    local itemInfo = Game.GetItemInfoByType(itemTypeId, nil, modules)
    local link = ChatLib.EncodeItemLink(itemTypeId, itemInfo.hidden_modules, itemInfo.slotted_modules)
    ChatLib.SystemMessage({text=link})
end


function ZoneInfo(args)
    Debug.Table("ZoneInfo", args)

    local zoneId = args[1] or args.text or 0
    Debug.Log("Current Zone: ", Game.GetZoneId())
    Debug.Table(Game.GetZoneInfo(Game.GetZoneId(zoneId)))
end




function ScanEntity(args)

    local targets = Game.GetAvailableTargets()

    for i, target in ipairs(targets) do
        local info = Game.GetTargetInfo(target)
        if info then
            Debug.Log(tostring(info.name) .. ' - ' .. tostring(target))
           
        end
        if not info.name then
            --Debug.Table(target, info)
        end
    end


end


function DumpEntity(args)

    local target = args.text

    local targetInfo = Game.GetTargetInfo(target)

    if targetInfo then

        Debug.Table("targetInfo", targetInfo)

        Debug.Table("targetBounds", Game.GetTargetBounds(target))

        if targetInfo.isNpc then
            Debug.Table("characterInfo", Game.GetTargetCharacterInfo(target))
        end


    else
        Debug.Log('DumpEntity no targetInfo on ' .. target)
    end



end



function getMyTargetId(args)
    Debug.Log("Player.GetTargetId(): " .. tostring(Player.GetTargetId()))
end



function typeId(args)
    Debug.Table('typeId', args)
    local typeId = args[1]
    assert(typeId, "you forgot the parameter")
    Debug.Table("Game.GetItemInfoByType("..typeId..")", Game.GetItemInfoByType(typeId))
end 


function subTypeId(args)
    Debug.Table('subTypeId', args)
    local subTypeId = args[1]
    assert(subTypeId, "you forgot the parameter")
    Debug.Table("Game.GetResourceTypeInfo("..subTypeId..")", Game.GetResourceTypeInfo(subTypeId))
end


function abilityId(args)
    Debug.Table('abilityId', args)
    local abilityId = args[1]
    assert(abilityId, "you forgot the parameter")
    Debug.Table("Player.GetAbilityInfo("..abilityId..")", Player.GetAbilityInfo(abilityId))
end

function itemId(args)
    Debug.Table('itemId', args)
    local itemId = args[1]
    assert(itemId, "you forgot the parameter")
    Debug.Table("Player.GetItemInfo("..itemId..")", Player.GetItemInfo(itemId))
end


function markerId(args)
    Debug.Table('markerId', args)
    local markerId = args[1]
    assert(markerId, "you forgot the parameter")
    Debug.Table("Game.GetMapMarkerInfo("..markerId..")", Game.GetMapMarkerInfo(markerId))
end

function encounterId(args)
    Debug.Table('encounterId', args)
    local encounterId = args[1]
    assert(encounterId, "you forgot the parameter")
    Debug.Table("Game.GetEncounterUiFields("..encounterId..")", Game.GetEncounterUiFields(encounterId))
end


function craftingTypeId(args)

    Debug.Error("craftingTypeId is not implemented")

    Debug.Table('craftingTypeId', args)
    local craftingTypeId = args[1]

    Debug.Log("CraftingTypeId function probably doesnt output the right stuff, wrong function because Im lazy")

    Debug.Table(Game.GetResourceTypeInfo(craftingTypeId))
end



